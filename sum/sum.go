package sum

import "fmt"

func AddIntegers(x int, y int) (int, error) {
	var z int
	if x != 0 && y != 0 {
		z = x + y
	}
	return z, nil
}

func AddMetrics(x [][]int, y [][]int) [][]int {
	var c [][]int
	if x != nil && y != nil {
		for i, a := range x {
			for j, _ := range a {
				c[i][j] = x[i][j] + y[i][j]
				fmt.Printf("Adding metric%d + metric%d = %d", x[i][j], y[i][j], c[i][j])
			}
		}
	}
	return c
}
