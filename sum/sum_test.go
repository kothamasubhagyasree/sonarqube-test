package sum_test

import (
	"coverageTesting/sum"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddIntegers(t *testing.T) {
	type args struct {
		a int
		b int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "should returned added number",
			args: args{10, 20},
			want: 30,
		},
		{
			name: "should returned an error",
			args: args{0, 20},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, _ := sum.AddIntegers(tt.args.a, tt.args.b)
			assert.Equal(t, tt.want, got)
		})
	}
}

/* func TestAddMetric(t *testing.T) {
	m1 := [][]int{
		{1, 3, 5}, {2, 4, 6}, {5, 10, 15},
	}
	m2 := [][]int{
		{11, 13, 15}, {12, 14, 16}, {15, 20, 25},
	}
	m3 := sum.AddMetrics(m1, m2)
	fmt.Println(m3)
} */
