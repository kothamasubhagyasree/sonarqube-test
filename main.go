package main

import (
	"coverageTesting/sum"
	"fmt"
)

func main() {
	fmt.Println("Main started...")
	n, _ := sum.AddIntegers(1, 2)
	fmt.Println(n)

	/* m1 := [][]int{
		{1, 3, 5}, {2, 4, 6}, {5, 10, 15},
	}
	m2 := [][]int{
		{11, 13, 15}, {12, 14, 16}, {15, 20, 25},
	}
	m3 := sum.AddMetrics(m1, m2)
	fmt.Println(m3) */
}
