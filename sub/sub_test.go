package sub_test

import (
	"coverageTesting/sub"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddIntegers(t *testing.T) {
	type args struct {
		a int
		b int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "should returned added number",
			args: args{30, 10},
			want: 20,
		},
		{
			name: "should returned an error",
			args: args{0, 20},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := sub.SubIntegers(tt.args.a, tt.args.b)
			assert.Equal(t, tt.want, got)
		})
	}
}
